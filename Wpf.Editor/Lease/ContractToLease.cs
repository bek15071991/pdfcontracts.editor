﻿using System.Windows;

namespace Wpf.Editor.Lease
{
    public class ContractToLease : Contract
    {
        protected WordToReplace CreteLordInitial(Point point, string key = null)
        {
            return CreteInitial(point, $"LordInitial{key}");
        }

        private WordToReplace CreteTenantInitial(Point point, string key = null)
        {
            return CreteInitial(point, $"TenantInitial{key}");
        } 

        public ContractToLease() : base($"Lease\\{nameof(ContractToLease)}.pdf", 3, 9.5f)
        {
            var page = Pages[1];

            #region page1 

            page.Add(new WordToReplace("ContractToLease", new[] {new Point(35, 88)}));
            page.Add(new WordToReplace("ProspectiveTenant", new[] {new Point(35, 123)}));
            page.Add(new WordToReplace("ProspectiveLandlord", new[] {new Point(55, 140)}));
            page.Add(new WordToReplace("ProspectiveLandlord1", new[] {new Point(272, 158)}));
            page.Add(new WordToReplace("ProspectiveLandlord2", new[] {new Point(35, 174)}));

            page.Add(new WordToReplace("Broker", new[] {new Point(145, 187)}));
            page.Add(new WordToReplace("Broker1", new[] {new Point(254, 187)}));

            page.Add(new WordToReplace("DepositReceipt", new[] {new Point(146, 205)}));
            page.Add(new WordToReplace("DepositReceiptAmount", new[] {new Point(271, 222)}));

            page.Add(new WordToReplace("BR", new[] {new Point(203, 251)}));
            page.Add(new WordToReplace("BA", new[] {new Point(262, 251)}));

            page.Add(new WordToReplace("DescriptionFinished", new[] {new Point(321, 251)}));
            page.Add(new WordToReplace("DescriptionUnFinished", new[] {new Point(379, 251)}));
            page.Add(new WordToReplace("DescriptionInventory", new[] {new Point(447, 251)}));

            page.Add(new WordToReplace("Parking", new[] {new Point(132, 268)}));
            page.Add(new WordToReplace("Parking1", new[] {new Point(307, 270)}));

            page.Add(new WordToReplace("Pets", new[] {new Point(85, 286)}));
            page.Add(new WordToReplace("Pets1", new[] {new Point(144, 286)}));
            page.Add(new WordToReplace("Pets2", new[] {new Point(254, 287)}));

            page.Add(new WordToReplace("Restrictions", new[] {new Point(184, 305)}));
            page.Add(new WordToReplace("Restrictions1", new[] {new Point(169, 320)}));
            page.Add(new WordToReplace("Restrictions2", new[] {new Point(340, 322)}));

            page.Add(new WordToReplace("Terms", new[] {new Point(299, 337)}));
            page.Add(new WordToReplace("Terms1", new[] {new Point(372, 337)}));
            page.Add(new WordToReplace("Terms2", new[] {new Point(493, 337)}));
            page.Add(new WordToReplace("Terms3", new[] {new Point(67, 354)}));
            page.Add(new WordToReplace("Terms4", new[] {new Point(139, 354)}));
            page.Add(new WordToReplace("Terms5", new[] {new Point(272, 354)}));

            page.Add(new WordToReplace("TermsTotal", new[] {new Point(109, 371)}));
            page.Add(new WordToReplace("TermsTotal1", new[] {new Point(211, 371)}));

            page.Add(new WordToReplace("TermsDeposit", new[] {new Point(152, 389)}));
            page.Add(new WordToReplace("TermsDeposit1", new[] {new Point(326, 389)}));
            page.Add(new WordToReplace("TermsDeposit2", new[] {new Point(440, 389)}));
            page.Add(new WordToReplace("TermsDeposit3", new[] {new Point(326, 406)}));
            page.Add(new WordToReplace("TermsDeposit4", new[] {new Point(440, 406)}));

            page.Add(new WordToReplace("TermsFirst", new[] {new Point(152, 423)}));
            page.Add(new WordToReplace("TermsSecond", new[] {new Point(152, 440)}));

            page.Add(new WordToReplace("TermsAdvance", new[] {new Point(152, 458)}));
            page.Add(new WordToReplace("TermsAdvance1", new[] {new Point(280, 458)}));
            page.Add(new WordToReplace("TermsAdvance2", new[] {new Point(440, 458)}));

            page.Add(new WordToReplace("TermsApplication", new[] {new Point(152, 475)}));
            page.Add(new WordToReplace("TermsApplication1", new[] {new Point(280, 475)}));
            page.Add(new WordToReplace("TermsApplication2", new[] {new Point(440, 475)}));

            page.Add(new WordToReplace("TermsBroker", new[] {new Point(374, 493)}));
            page.Add(new WordToReplace("TermsLandlord", new[] {new Point(422, 493)}));
            page.Add(new WordToReplace("TermsOther", new[] {new Point(545, 493)}));
            page.Add(new WordToReplace("TermsSeparate", new[] {new Point(53, 510)}));
            page.Add(new WordToReplace("TermsInterest", new[] {new Point(212, 510)}));
            page.Add(new WordToReplace("TermsNonInterest", new[] {new Point(297, 510)}));


            page.Add(new WordToReplace("ExpensesElectric", new[] {new Point(161, 556)}));
            page.Add(new WordToReplace("ExpensesElectric1", new[] {new Point(204, 556)}));
            page.Add(new WordToReplace("ExpensesElectric2", new[] {new Point(236, 556)}));
            page.Add(new WordToReplace("ExpensesHeating", new[] {new Point(413, 556)}));
            page.Add(new WordToReplace("ExpensesHeating1", new[] {new Point(456, 556)}));
            page.Add(new WordToReplace("ExpensesHeating2", new[] {new Point(488, 556)}));

            page.Add(new WordToReplace("ExpensesGas", new[] {new Point(161, 568)}));
            page.Add(new WordToReplace("ExpensesGas1", new[] {new Point(204, 568)}));
            page.Add(new WordToReplace("ExpensesGas2", new[] {new Point(236, 568)}));
            page.Add(new WordToReplace("ExpensesCeiling", new[] {new Point(413, 568)}));
            page.Add(new WordToReplace("ExpensesCeiling1", new[] {new Point(456, 568)}));
            page.Add(new WordToReplace("ExpensesCeiling2", new[] {new Point(488, 568)}));


            page.Add(new WordToReplace("ExpensesSewer", new[] {new Point(161, 579)}));
            page.Add(new WordToReplace("ExpensesSewer1", new[] {new Point(204, 579)}));
            page.Add(new WordToReplace("ExpensesSewer2", new[] {new Point(236, 579)}));
            page.Add(new WordToReplace("ExpensesRoof", new[] {new Point(413, 579)}));
            page.Add(new WordToReplace("ExpensesRoof1", new[] {new Point(456, 579)}));
            page.Add(new WordToReplace("ExpensesRoof2", new[] {new Point(488, 579)}));


            page.Add(new WordToReplace("ExpensesWater", new[] {new Point(161, 590)}));
            page.Add(new WordToReplace("ExpensesWater1", new[] {new Point(204, 590)}));
            page.Add(new WordToReplace("ExpensesWater2", new[] {new Point(236, 590)}));
            page.Add(new WordToReplace("ExpensesLawn", new[] {new Point(413, 590)}));
            page.Add(new WordToReplace("ExpensesLawn1", new[] {new Point(456, 590)}));
            page.Add(new WordToReplace("ExpensesLawn2", new[] {new Point(488, 590)}));


            page.Add(new WordToReplace("ExpensesTrash", new[] {new Point(161, 601)}));
            page.Add(new WordToReplace("ExpensesTrash1", new[] {new Point(204, 601)}));
            page.Add(new WordToReplace("ExpensesTrash2", new[] {new Point(236, 601)}));
            page.Add(new WordToReplace("ExpensesPool", new[] {new Point(413, 602)}));
            page.Add(new WordToReplace("ExpensesPool1", new[] {new Point(456, 602)}));
            page.Add(new WordToReplace("ExpensesPool2", new[] {new Point(488, 602)}));


            page.Add(new WordToReplace("ExpensesTelephone", new[] {new Point(161, 613)}));
            page.Add(new WordToReplace("ExpensesTelephone1", new[] {new Point(204, 613)}));
            page.Add(new WordToReplace("ExpensesTelephone2", new[] {new Point(236, 613)}));
            page.Add(new WordToReplace("ExpensesPest", new[] {new Point(413, 613)}));
            page.Add(new WordToReplace("ExpensesPest1", new[] {new Point(456, 613)}));
            page.Add(new WordToReplace("ExpensesPest2", new[] {new Point(488, 613)}));

            page.Add(new WordToReplace("ExpensesHotWater", new[] {new Point(413, 625)}));
            page.Add(new WordToReplace("ExpensesHotWater1", new[] {new Point(456, 625)}));
            page.Add(new WordToReplace("ExpensesHotWater2", new[] {new Point(488, 625)}));

            page.Add(new WordToReplace("ExpensesWindows", new[] {new Point(413, 636)}));
            page.Add(new WordToReplace("ExpensesWindows1", new[] {new Point(456, 636)}));
            page.Add(new WordToReplace("ExpensesWindows2", new[] {new Point(488, 636)}));

            page.Add(new WordToReplace("ExpensesElectrical", new[] {new Point(413, 648)}));
            page.Add(new WordToReplace("ExpensesElectrical1", new[] {new Point(456, 648)}));
            page.Add(new WordToReplace("ExpensesElectrical2", new[] {new Point(488, 648)}));

            page.Add(new WordToReplace("ExpensesGarbage", new[] {new Point(413, 659)}));
            page.Add(new WordToReplace("ExpensesGarbage1", new[] {new Point(456, 660)}));
            page.Add(new WordToReplace("ExpensesGarbage2", new[] {new Point(488, 660)}));

            page.Add(new WordToReplace("ExpensesStructure", new[] {new Point(413, 671)}));
            page.Add(new WordToReplace("ExpensesStructure1", new[] {new Point(456, 671)}));
            page.Add(new WordToReplace("ExpensesStructure2", new[] {new Point(488, 671)}));

            page.Add(new WordToReplace("ExpensesSmoke", new[] {new Point(413, 682)}));
            page.Add(new WordToReplace("ExpensesSmoke1", new[] {new Point(456, 682)}));
            page.Add(new WordToReplace("ExpensesSmoke2", new[] {new Point(488, 682)}));

            page.Add(new WordToReplace("ExpensesLocks", new[] {new Point(413, 694)}));
            page.Add(new WordToReplace("ExpensesLocks1", new[] {new Point(456, 694)}));
            page.Add(new WordToReplace("ExpensesLocks2", new[] {new Point(488, 694)}));

            page.Add(CreteLordInitial(new Point(122, 715)));
            page.Add(CreteLordInitial(new Point(152, 715), "1"));

            page.Add(CreteTenantInitial(new Point(273, 715)));
            page.Add(CreteTenantInitial(new Point(305, 715), "1")); 
            #endregion

            page = Pages[2];

            #region page2

            page.Add(new WordToReplace("TaxesProperty", new[] {new Point(160, 57)}));
            page.Add(new WordToReplace("TaxesProperty1", new[] {new Point(204, 57)}));
            page.Add(new WordToReplace("TaxesProperty2", new[] {new Point(237, 57)}));

            page.Add(new WordToReplace("TaxesSales", new[] {new Point(160, 69)}));
            page.Add(new WordToReplace("TaxesSales1", new[] {new Point(204, 69)}));
            page.Add(new WordToReplace("TaxesSales2", new[] {new Point(237, 69)}));

            page.Add(new WordToReplace("TaxesPersonal", new[] {new Point(160, 81)}));
            page.Add(new WordToReplace("TaxesPersonal1", new[] {new Point(204, 81)}));
            page.Add(new WordToReplace("TaxesPersonal2", new[] {new Point(237, 81)}));
            page.Add(new WordToReplace("InsurancePersonal", new[] {new Point(413, 80)}));
            page.Add(new WordToReplace("InsurancePersonal1", new[] {new Point(456, 80)}));
            page.Add(new WordToReplace("InsurancePersonal2", new[] {new Point(489, 80)}));

            page.Add(new WordToReplace("TaxesIntangible", new[] {new Point(160, 92)}));
            page.Add(new WordToReplace("TaxesIntangible1", new[] {new Point(204, 92)}));
            page.Add(new WordToReplace("TaxesIntangible2", new[] {new Point(237, 92)}));
            page.Add(new WordToReplace("InsuranceProperty", new[] {new Point(413, 92)}));
            page.Add(new WordToReplace("InsuranceProperty1", new[] {new Point(456, 92)}));
            page.Add(new WordToReplace("InsuranceProperty2", new[] {new Point(489, 92)}));

            page.Add(new WordToReplace("InsuranceFlood", new[] {new Point(413, 103)}));
            page.Add(new WordToReplace("InsuranceFlood1", new[] {new Point(456, 103)}));
            page.Add(new WordToReplace("InsuranceFlood2", new[] {new Point(489, 103)}));

            page.Add(new WordToReplace("TaxesOther", new[] {new Point(78, 118)}));
            page.Add(new WordToReplace("TaxesOther1", new[] {new Point(161, 115)}));
            page.Add(new WordToReplace("TaxesOther2", new[] {new Point(204, 115)}));
            page.Add(new WordToReplace("TaxesOther3", new[] {new Point(237, 115)}));
            page.Add(new WordToReplace("TaxesOther4", new[] {new Point(290, 118)}));
            page.Add(new WordToReplace("TaxesOther5", new[] {new Point(413, 115)}));
            page.Add(new WordToReplace("TaxesOther6", new[] {new Point(456, 115)}));
            page.Add(new WordToReplace("TaxesOther7", new[] {new Point(489, 115)}));


            page.Add(new WordToReplace("Preparation", new[] {new Point(135, 184)}));
            page.Add(new WordToReplace("Preparation1", new[] {new Point(238, 184)}));

            page.Add(new WordToReplace("Association", new[] {new Point(222, 242)}));
            page.Add(new WordToReplace("Association1", new[] {new Point(37, 254)}));
            page.Add(new WordToReplace("Association2", new[] {new Point(196, 289)}));
            page.Add(new WordToReplace("Association3", new[] {new Point(226, 289)}));

            page.Add(new WordToReplace("Brokerage", new[] {new Point(279, 621)}));
            page.Add(new WordToReplace("Brokerage1", new[] {new Point(397, 621)}));

            page.Add(CreteLordInitial(new Point(122, 710)));
            page.Add(CreteLordInitial(new Point(152, 710), "1"));

            page.Add(CreteTenantInitial(new Point(273, 710)));
            page.Add(CreteTenantInitial(new Point(305, 710), "1")); 
            #endregion

            page = Pages[3];

            #region page3

            page.Add(new WordToReplace("Special", new[] {new Point(160, 46)}));
            page.Add(new WordToReplace("Special1", new[] {new Point(38, 63)}));
            page.Add(new WordToReplace("Special2", new[] {new Point(38, 80)}));
            page.Add(new WordToReplace("Special3", new[] {new Point(38, 98)}));
            page.Add(new WordToReplace("Special4", new[] {new Point(38, 115)}));
            page.Add(new WordToReplace("Special5", new[] {new Point(38, 132)}));
            page.Add(new WordToReplace("Special6", new[] {new Point(38, 149)}));
            page.Add(new WordToReplace("Special7", new[] {new Point(38, 166)}));
            page.Add(new WordToReplace("Special8", new[] {new Point(38, 184)}));
            page.Add(new WordToReplace("Special9", new[] {new Point(38, 201)}));
            page.Add(new WordToReplace("Special10", new[] {new Point(38, 218)}));

            page.Add(new WordToReplace("Date1", new[] {new Point(65, 297)}));
            page.Add(new WordToReplace("ProspectiveTenantInitial", new[] {new Point(213, 297)}));
            page.Add(new WordToReplace("TaxId1", new[] {new Point(430, 297)}));

            page.Add(new WordToReplace("Date2", new[] {new Point(65, 320)}));
            page.Add(new WordToReplace("ProspectiveTenantInitial1", new[] {new Point(213, 320)}));
            page.Add(new WordToReplace("TaxId2", new[] {new Point(430, 320)}));

            page.Add(new WordToReplace("Home", new[] {new Point(120, 343)}));
            page.Add(new WordToReplace("Work", new[] {new Point(335, 343)}));
            page.Add(new WordToReplace("Facsimile", new[] {new Point(488, 343)}));

            page.Add(new WordToReplace("Address", new[] {new Point(80, 366)}));
            page.Add(new WordToReplace("Address1", new[] {new Point(38, 389)}));

            page.Add(new WordToReplace("Email", new[] {new Point(330, 389)}));

            page.Add(new WordToReplace("Date3", new[] {new Point(65, 412)}));
            page.Add(new WordToReplace("ProspectiveLandlordInitial", new[] {new Point(290, 412)}));

            page.Add(new WordToReplace("Date4", new[] {new Point(65, 435)}));
            page.Add(new WordToReplace("ProspectiveLandlordInitial1", new[] {new Point(290, 435)}));

            page.Add(CreteLordInitial(new Point(122, 705)));
            page.Add(CreteLordInitial(new Point(152, 705), "1"));

            page.Add(CreteTenantInitial(new Point(273, 705)));
            page.Add(CreteTenantInitial(new Point(303, 705), "1"));

            #endregion
        }
    }
}