﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Resources;
using iTextSharp.text;
using iTextSharp.text.pdf;
using JetBrains.Annotations;
using Microsoft.Win32;

namespace Wpf.Editor
{
    public class PdfService
    {
        public void ReplaceWords([NotNull] Contract contract, IDictionary<string, object> replaceTexts)
        {
            if (contract == null)
                throw new ArgumentNullException(nameof(contract));

            var pathIn = contract.PdfPath;

            var fontSize = contract.FontSize;

            var saveFileDialog = new SaveFileDialog
            {
                Filter = "Pdf file (*.pdf)|*.pdf",
                InitialDirectory = Environment.CurrentDirectory
            };

            if (saveFileDialog.ShowDialog() == false)
                return;

            var pathOut = saveFileDialog.FileName;

            using (var reader = new PdfReader(pathIn))
            using (var stamper = new PdfStamper(reader, new FileStream(pathOut, FileMode.Create)))
            {
                var pageSize = reader.GetPageSize(1);

                reader.SelectPages($"{1}-{contract.PageCount}");

                foreach (var page in contract.Pages)
                {
                    var content = stamper.GetOverContent(page.Key);

                    foreach (var word in page.Value)
                    {
                        if (!replaceTexts.TryGetValue(word.Key, out var replaceText))
                            throw new InvalidOperationException($"Cannot found word#{word.Key}");

                        Font font;

                        if (word.Key.Contains("Initial") || word.Key.Contains("Signature") || word.Key.Contains("Sign"))
                        {
                            font = GetCursiveFont(fontSize);
                        }
                        else
                            font = new Font {Size = fontSize};

                        foreach (var wordPoint in word.Points)
                        {
                            var x = Convert.ToSingle(wordPoint.X);
                            var y = (int) (pageSize.Height - wordPoint.Y);

                            var text = string.Empty;

                            if (replaceText is bool boolText)
                            {
                                if (boolText)
                                    text = "X";
                            }
                            else
                            {
                                text = replaceText.ToString();
                            }

                            var phrase = new Phrase(str: text, font: font);

                            ColumnText.ShowTextAligned(content, Element.ALIGN_LEFT, phrase, x, y, 0);
                        }
                    }
                }
            }

            Open(pathOut);
        }

        private static void Open(string path)
        {
            System.Diagnostics.Process.Start(@path);
        }

        private static Font GetCursiveFont(float fontSize)
        {
            var fontBytes = Properties.Resources.VanillaDaisy_SS00;

            var customFont = BaseFont.CreateFont("Fonts\\VanillaDaisy-SS00.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED,
                BaseFont.CACHED, fontBytes, null);
             
            fontSize += 2;

            var font = new Font(customFont, fontSize);

            font.SetStyle("italic");
            font.SetStyle("bold");

            return font;
        }

        private static string ReadResourceFile(string filename)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();
            using (var stream = thisAssembly.GetManifestResourceStream(filename))
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}