﻿using System.Windows;
using System.Windows.Data;

namespace Wpf.Editor.AsIsResidential
{ 
    public partial class AsIsResidentialUserControl 
    {
        public string BayerInitial
        {
            get => (string)GetValue(BayerInitialProperty);
            set => SetValue(BayerInitialProperty, value);
        }

        public static readonly DependencyProperty BayerInitialProperty = DependencyProperty.Register(
            nameof(BayerInitial),
            typeof(string),
            typeof(AsIsResidentialUserControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
                { DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged }); 

        public string BayerInitial1
        {
            get => (string)GetValue(BayerInitial1Property);
            set => SetValue(BayerInitial1Property, value);
        }

        public static readonly DependencyProperty BayerInitial1Property = DependencyProperty.Register(
            nameof(BayerInitial1),
            typeof(string),
            typeof(AsIsResidentialUserControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
                { DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });


        public string SellerInitial
        {
            get => (string)GetValue(SellerInitialProperty);
            set => SetValue(SellerInitialProperty, value);
        }

        public static readonly DependencyProperty SellerInitialProperty = DependencyProperty.Register(
            nameof(SellerInitial),
            typeof(string),
            typeof(AsIsResidentialUserControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
                { DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged }); 

        public string SellerInitial1
        {
            get => (string)GetValue(SellerInitial1Property);
            set => SetValue(SellerInitial1Property, value);
        }

        public static readonly DependencyProperty SellerInitial1Property = DependencyProperty.Register(
            nameof(SellerInitial1),
            typeof(string),
            typeof(AsIsResidentialUserControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
                { DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });


        public AsIsResidentialUserControl()
        {
            InitializeComponent();
        }
    }
}
