﻿using System.Windows;

namespace Wpf.Editor.AsIsResidential
{
    public class AsIsResidentialContract : Contract
    {
        protected WordToReplace CreteSellerInitial(Point point, string key = null)
        {
            return CreteInitial(point, $"SellerInitial{key}");
        }

        protected WordToReplace CreteBayerInitial(Point point, string key = null)
        {
            return CreteInitial(point, $"BayerInitial{key}");
        }


        public AsIsResidentialContract()
            : base($"AsIsResidential\\{nameof(AsIsResidentialContract)}.pdf", 12, 10)
        {
            var page = Pages[1];

            #region page1

            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            page.Add(new WordToReplace("Seller", new[] {new Point(102, 96)}));
            page.Add(new WordToReplace("Buyer", new[] {new Point(78, 108)}));
            page.Add(new WordToReplace("Address", new[] {new Point(200, 166)}));
            page.Add(new WordToReplace("Country", new[] {new Point(142, 177)}));
            page.Add(new WordToReplace("TaxId", new[] {new Point(380, 177)}));
            page.Add(new WordToReplace("Description", new[] {new Point(260, 189)}));
            page.Add(new WordToReplace("Description1", new[] {new Point(90, 200)}));
            page.Add(new WordToReplace("Description2", new[] {new Point(90, 212)}));
            page.Add(new WordToReplace("PersonalProperty", new[] {new Point(365, 315)}));
            page.Add(new WordToReplace("PersonalProperty1", new[] {new Point(90, 326)}));
            page.Add(new WordToReplace("Purchase", new[] {new Point(330, 349)}));
            page.Add(new WordToReplace("Purchase1", new[] {new Point(90, 360)}));
            page.Add(new WordToReplace("PurchasePrice", new[] {new Point(505, 396)}));
            page.Add(new WordToReplace("PurchasePriceA", new[] {new Point(505, 414)}));
            page.Add(new WordToReplace("PurchasePriceAI", new[] {new Point(177, 437)}));
            page.Add(new WordToReplace("PurchasePriceAII", new[] {new Point(300, 437)}));
            page.Add(new WordToReplace("PurchasePriceAWithin", new[] {new Point(402, 437)}));
            page.Add(new WordToReplace("PurchasePriceAName", new[] {new Point(242, 471)}));
            page.Add(new WordToReplace("PurchasePriceAAddress", new[] {new Point(135, 483)}));
            page.Add(new WordToReplace("PurchasePriceAPhone", new[] {new Point(125, 494)}));
            page.Add(new WordToReplace("PurchasePriceAEmail", new[] {new Point(250, 494)}));
            page.Add(new WordToReplace("PurchasePriceAFax", new[] {new Point(408, 494)}));
            page.Add(new WordToReplace("PurchasePriceBWithin", new[] {new Point(345, 506)}));
            page.Add(new WordToReplace("PurchasePriceBDate", new[] {new Point(505, 517)}));
            page.Add(new WordToReplace("PurchasePriceC", new[] {new Point(505, 539)}));
            page.Add(new WordToReplace("PurchasePriceD", new[] {new Point(123, 558)}));
            page.Add(new WordToReplace("PurchasePriceD1", new[] {new Point(505, 558)}));
            page.Add(new WordToReplace("PurchasePriceE", new[] {new Point(505, 580)}));
            page.Add(new WordToReplace("TimeFor", new[] {new Point(90, 626)}));
            page.Add(new WordToReplace("ClosingDate", new[] {new Point(137, 706)}));

            #endregion


            page = Pages[2];

            #region page2

            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            page.Add(new WordToReplace("OccupancyB", new[] {new Point(92, 234)}));
            page.Add(new WordToReplace("Assignability", new[] {new Point(263, 337)}));
            page.Add(new WordToReplace("Assignability1", new[] {new Point(140, 349)}));
            page.Add(new WordToReplace("Assignability2", new[] {new Point(474, 349)}));
            page.Add(new WordToReplace("FinancingA", new[] {new Point(74, 407)}));
            page.Add(new WordToReplace("FinancingB", new[] {new Point(74, 453)}));
            page.Add(new WordToReplace("FinancingBConventional", new[] {new Point(393, 453)}));
            page.Add(new WordToReplace("FinancingBFha", new[] {new Point(465, 453)}));
            page.Add(new WordToReplace("FinancingBVa", new[] {new Point(500, 453)}));
            page.Add(new WordToReplace("FinancingBOther", new[] {new Point(543, 453)}));
            page.Add(new WordToReplace("FinancingBDescribe", new[] {new Point(73, 465)}));
            page.Add(new WordToReplace("FinancingBWithin", new[] {new Point(253, 465)}));
            page.Add(new WordToReplace("FinancingBFixed", new[] {new Point(200, 476)}));
            page.Add(new WordToReplace("FinancingBAdjustable", new[] {new Point(237, 476)}));
            page.Add(new WordToReplace("FinancingBOr", new[] {new Point(298, 476)}));
            page.Add(new WordToReplace("FinancingBExceed", new[] {new Point(278, 488)}));
            page.Add(new WordToReplace("FinancingBTermOf", new[] {new Point(230, 499)}));
            page.Add(new WordToReplace("FinancingBIWithin", new[] {new Point(417, 510)}));

            #endregion


            page = Pages[3];

            #region page3

            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            page.Add(new WordToReplace("FinancingC", new[] {new Point(74, 223)}));
            page.Add(new WordToReplace("FinancingD", new[] {new Point(74, 235)}));
            page.Add(new WordToReplace("ClosingA", new[] {new Point(400, 326)}));
            page.Add(new WordToReplace("ClosingB", new[] {new Point(110, 477)}));
            page.Add(new WordToReplace("ClosingC", new[] {new Point(301, 489)}));
            page.Add(new WordToReplace("FinancingCI", new[] {new Point(92, 638)}));
            page.Add(new WordToReplace("FinancingCII", new[] {new Point(92, 684)}));

            #endregion


            page = Pages[4];

            #region page4

            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            page.Add(new WordToReplace("FinancingCIII", new[] {new Point(92, 63)}));
            page.Add(new WordToReplace("FinancingCIII1", new[] {new Point(506, 108)}));
            page.Add(new WordToReplace("FinancingEBuyer", new[] {new Point(246, 164)}));
            page.Add(new WordToReplace("FinancingESeller", new[] {new Point(288, 164)}));
            page.Add(new WordToReplace("FinancingENA", new[] {new Point(329, 164)}));
            page.Add(new WordToReplace("FinancingEBy", new[] {new Point(92, 176)}));
            page.Add(new WordToReplace("FinancingEExceed", new[] {new Point(445, 176)}));
            page.Add(new WordToReplace("FinancingFA", new[] {new Point(93, 281)}));
            page.Add(new WordToReplace("FinancingFB", new[] {new Point(93, 303)}));
            page.Add(new WordToReplace("DisclosuresD", new[] {new Point(405, 602)}));

            #endregion

            page = Pages[5];

            #region page5
            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            page.Add(new WordToReplace("PropertyA", new[] {new Point(434, 429)}));

            #endregion

            page = Pages[6];

            #region page6
            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            #endregion

            page = Pages[7];

            #region page7
            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            #endregion

            page = Pages[8];

            #region page8
            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            #endregion

            page = Pages[9];

            #region page9
            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            #endregion

            page = Pages[10];

            #region page10
            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            #endregion

            page = Pages[11];

            #region page11
            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));


            page.Add(new WordToReplace("AddendaA", new[] {new Point(63, 290)}));
            page.Add(new WordToReplace("AddendaK", new[] {new Point(218, 290)}));
            page.Add(new WordToReplace("AddendaT", new[] {new Point(405, 290)}));

            page.Add(new WordToReplace("AddendaB", new[] {new Point(63, 301)}));
            page.Add(new WordToReplace("AddendaL", new[] {new Point(218, 301)}));
            page.Add(new WordToReplace("AddendaU", new[] {new Point(405, 301)}));

            page.Add(new WordToReplace("AddendaC", new[] {new Point(63, 311)}));
            page.Add(new WordToReplace("AddendaM", new[] {new Point(218, 311)}));
            page.Add(new WordToReplace("AddendaV", new[] {new Point(405, 311)}));

            page.Add(new WordToReplace("AddendaD", new[] {new Point(63, 322)}));
            page.Add(new WordToReplace("AddendaN", new[] {new Point(218, 322)}));
            page.Add(new WordToReplace("AddendaW", new[] {new Point(405, 322)}));

            page.Add(new WordToReplace("AddendaE", new[] {new Point(63, 333)}));
            page.Add(new WordToReplace("AddendaX", new[] {new Point(405, 333)}));

            page.Add(new WordToReplace("AddendaF", new[] {new Point(63, 344)}));
            page.Add(new WordToReplace("AddendaO", new[] {new Point(218, 344)}));
            page.Add(new WordToReplace("AddendaY", new[] {new Point(405, 344)}));

            page.Add(new WordToReplace("AddendaG", new[] {new Point(63, 355)}));
            page.Add(new WordToReplace("AddendaP", new[] {new Point(218, 355)}));
            page.Add(new WordToReplace("AddendaZ", new[] {new Point(405, 355)}));

            page.Add(new WordToReplace("AddendaH", new[] {new Point(63, 365)}));
            page.Add(new WordToReplace("AddendaQ", new[] {new Point(218, 365)}));
            page.Add(new WordToReplace("AddendaAA", new[] {new Point(405, 365)}));

            page.Add(new WordToReplace("AddendaI", new[] { new Point(63, 376) }));
            page.Add(new WordToReplace("AddendaR", new[] { new Point(218,376) }));
            page.Add(new WordToReplace("AddendaBB", new[] {new Point(405,376) }));

            page.Add(new WordToReplace("AddendaJ", new[] { new Point(63,387 ) }));
            page.Add(new WordToReplace("AddendaS", new[] { new Point(218,387) }));
            page.Add(new WordToReplace("AddendaCC", new[] {new Point(405,387) }));

            page.Add(new WordToReplace("AddendaOther", new[] { new Point(405,420)})); 
            page.Add(new WordToReplace("AddendaOther1", new[] { new Point(445,419) }));
            page.Add(new WordToReplace("AddendaOther2", new[] { new Point(445,431) })); 
            page.Add(new WordToReplace("AddendaOther3", new[] { new Point(445, 443) }));

            page.Add(new WordToReplace("Additional", new[] { new Point(178,471) }));
            page.Add(new WordToReplace("Additional1", new[] { new Point(72,483) }));
            page.Add(new WordToReplace("Additional2", new[] { new Point(72,494) }));
            page.Add(new WordToReplace("Additional3", new[] { new Point(72,506) }));
            page.Add(new WordToReplace("Additional4", new[] { new Point(72,517) }));
            page.Add(new WordToReplace("Additional5", new[] { new Point(72,529) }));
            page.Add(new WordToReplace("Additional6", new[] { new Point(72,540) }));
            page.Add(new WordToReplace("Additional7", new[] { new Point(72,552) }));
            page.Add(new WordToReplace("Additional8", new[] { new Point(72,563) }));
            page.Add(new WordToReplace("Additional9", new[] { new Point(72,575) }));
            page.Add(new WordToReplace("Additional10", new[] { new Point(72,586) }));
            page.Add(new WordToReplace("Additional11", new[] { new Point(72,598) }));
            page.Add(new WordToReplace("Additional12", new[] { new Point(72,609) }));
            page.Add(new WordToReplace("Additional13", new[] { new Point(72,621) }));
            page.Add(new WordToReplace("Additional14", new[] { new Point(72,633) }));
            page.Add(new WordToReplace("Additional15", new[] { new Point(72,644) }));
            page.Add(new WordToReplace("Additional16", new[] { new Point(72,656) }));
            page.Add(new WordToReplace("AdditionalCounter", new[] { new Point(74,697) }));
            page.Add(new WordToReplace("AdditionalCounter1", new[] { new Point(74,720) }));

            #endregion

            page = Pages[12];

            #region page12

            page.Add(CreteBayerInitial(new Point(108, 743)));
            page.Add(CreteBayerInitial(new Point(153, 743), "1"));

            page.Add(CreteSellerInitial(new Point(483, 743)));
            page.Add(CreteSellerInitial(new Point(532, 743), "1"));



            page.Add(new WordToReplace("AdditionalBuyerInitial", new[] { new Point(103, 213) }));
            page.Add(new WordToReplace("AdditionalDate", new[] { new Point(485, 213) }));
            page.Add(new WordToReplace("AdditionalBuyerInitial1", new[] { new Point(103, 236) }));
            page.Add(new WordToReplace("AdditionalDate1", new[] { new Point(485, 236) }));
            page.Add(new WordToReplace("AdditionalSellerInitial", new[] { new Point(103, 259) }));
            page.Add(new WordToReplace("AdditionalDate2", new[] { new Point(485, 259) }));
            page.Add(new WordToReplace("AdditionalSellerInitial1", new[] { new Point(103, 282) }));
            page.Add(new WordToReplace("AdditionalDate3", new[] { new Point(485, 282) }));

            page.Add(new WordToReplace("AdditionalBuyerAddress", new[] { new Point(73, 317) }));
            page.Add(new WordToReplace("AdditionalSellerAddress", new[] { new Point(325, 317) }));

            page.Add(new WordToReplace("AdditionalBuyerAddress1", new[] { new Point(73, 328) }));
            page.Add(new WordToReplace("AdditionalSellerAddress1", new[] { new Point(325, 328) }));

            page.Add(new WordToReplace("AdditionalBuyerAddress2", new[] { new Point(73, 339) }));
            page.Add(new WordToReplace("AdditionalSellerAddress2", new[] { new Point(325, 339) }));

            page.Add(new WordToReplace("AdditionalCooperatingSales", new[] { new Point(73, 443) }));
            page.Add(new WordToReplace("AdditionalListingSales", new[] { new Point(340, 443) }));
            page.Add(new WordToReplace("AdditionalCooperatingBroker", new[] { new Point(73, 478) }));
            page.Add(new WordToReplace("AdditionalListingBroker", new[] { new Point(340, 478) }));

            #endregion 
        }
    }
}