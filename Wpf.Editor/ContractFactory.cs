﻿using System;
using Wpf.Editor.AsIsResidential;
using Wpf.Editor.Lease;
using Wpf.Editor.MonthToLease;
using Wpf.Editor.ResidentialMulti;
using Wpf.Editor.ResidentialSingle;

namespace Wpf.Editor
{
    public class ContractFactory
    {
        public Contract Create(ContractType type)
        {
            switch (type)
            {
                case ContractType.ContractToLease: return new ContractToLease();
                case ContractType.MonthLease: return new ContractMonthToLease();
                case ContractType.AsIsResidential: return new AsIsResidentialContract();
                case ContractType.ResidentialLeaseForMulti: return new ResidentialLeaseForMulti();
                case ContractType.ResidentialLeaseForSingle: return new ResidentialLeaseForSingle();
                default: throw new InvalidOperationException($"Cannot define contract type#{type}");
            }
        }
    }
}