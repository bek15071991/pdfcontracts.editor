﻿using System;
using System.Collections.Generic;
using System.Linq; 
using JetBrains.Annotations; 
using System.Windows;

namespace Wpf.Editor
{
    public class WordToReplace
    {
        public WordToReplace([NotNull] string key, [NotNull] IEnumerable<Point> points)
        {
            Key = key ?? throw new ArgumentNullException(nameof(key)); 

            if (points == null)
                throw new ArgumentNullException(nameof(points));

            Points = points.ToList();
        }

        public string Key { get; } 

        public IReadOnlyList<Point> Points { get; }
    }
}