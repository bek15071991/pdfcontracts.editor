﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Wpf.Editor.AsIsResidential;
using Wpf.Editor.MonthToLease;
using Wpf.Editor.ResidentialMulti;
using Wpf.Editor.ResidentialSingle;

namespace Wpf.Editor
{
    public partial class MainWindow
    {
        private readonly PdfService _pdfService = new PdfService();
        private readonly ContractFactory _contractFactory = new ContractFactory();
        private ContractType _currentContractType;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Show(FrameworkElement userControl)
        {
            if (MainView.Content != null)
            {
                if (MessageBox.Show("Close the current contract?", "Attention", MessageBoxButton.YesNo) ==
                    MessageBoxResult.No)
                    return;
            }

            Title = userControl.Tag?.ToString() ?? string.Empty;

            ScrollViewer.ScrollToTop();

            MainView.Content = userControl;
        }

        private static IEnumerable<Control> GetControls(ContentControl userControl)
        {
            if (userControl == null)
                return new List<Control>();

            var controls = new List<Control>();

            var grid = (Grid) userControl.Content;

            foreach (var gridChild in grid.Children)
            {
                if (TryGet(gridChild, out var element))
                {
                    controls.Add(element);
                    continue;
                }

                if (!(gridChild is StackPanel stackPanel))
                    continue;

                foreach (var stackChild in stackPanel.Children)
                {
                    if (stackChild is StackPanel panel)
                    {
                        foreach (var panelChild in panel.Children)
                        {
                            if (!TryGet(panelChild, out var c))
                                continue;

                            controls.Add(c);
                        }

                        continue;
                    }

                    if (!TryGet(stackChild, out var control))
                        continue;

                    controls.Add(control);
                }
            }

            return controls;

            bool TryGet(object element, out Control c)
            {
                c = null;
                switch (element)
                {
                    case TextBox textBox:
                        c = textBox;
                        return true;
                    case CheckBox checkBox:
                        c = checkBox;
                        return true;
                    default:
                        return false;
                }
            }
        }

        private void Save_OnClick(object sender, RoutedEventArgs e)
        {
            if (MainView?.Content == null)
                return;

            try
            {
                var contract = _contractFactory.Create(_currentContractType);

                var dictText = new Dictionary<string, object>();

                var controls = GetControls(MainView.Content as ContentControl);

                var data = new Dictionary<string, object>();

                foreach (var control in controls)
                {
                    var key = string.Empty;
                    object value;

                    if (control is TextBox textBox)
                    {
                        key = textBox.Tag.ToString();
                        value = textBox.Text;
                    }
                    else
                    {
                        var checkBox = ((CheckBox)control);

                        key = checkBox.Tag.ToString();
                        value = checkBox.IsChecked;
                    }

                    if (data.ContainsKey(key) && !key.Contains("Initial"))
                        throw new InvalidOperationException($"Contains key '{key}'");

                    data[key] = value;
                }

                foreach (var page in contract.Pages)
                {
                    foreach (var wordToReplace in page.Value)
                    {
                        if (!data.TryGetValue(wordToReplace.Key.Trim(), out var replacedText))
                        {
                            throw new InvalidOperationException($"Cannot found word#{wordToReplace.Key.Trim()}");
                        }

                        dictText[wordToReplace.Key] = replacedText;
                    }
                }

                _pdfService.ReplaceWords(contract, dictText);

                MessageBox.Show("Contract is created");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            } 
        }

        private void Lease_OnClick(object sender, RoutedEventArgs e)
        {
            Show(new Lease.ContractUserControl());
            _currentContractType = ContractType.ContractToLease;
        }

        private void MonthTo_OnClick(object sender, RoutedEventArgs e)
        {
            Show(new ContractMonthToLeaseUserControl());
            _currentContractType = ContractType.MonthLease;
        }

        private void AsIsResidential_OnClick(object sender, RoutedEventArgs e)
        {
            Show(new AsIsResidentialUserControl());
            _currentContractType = ContractType.AsIsResidential;
        }

        private void ResidentialMulti_OnClick(object sender, RoutedEventArgs e)
        { 
            Show(new ResidentialLeaseForMultiUserControl());
            _currentContractType = ContractType.ResidentialLeaseForMulti;
        }

        private void ResidentialSingle_OnClick(object sender, RoutedEventArgs e)
        {
            Show(new ResidentialLeaseForSingleUserControl());
            _currentContractType = ContractType.ResidentialLeaseForSingle; 
        }
    }
}