﻿using System.Windows;

namespace Wpf.Editor.MonthToLease
{
    public class ContractMonthToLease : Contract
    {
        protected WordToReplace CreteLordInitial(Point point, string key = null)
        {
            return CreteInitial(point, $"LordInitial{key}");
        }

        private WordToReplace CreteTenantInitial(Point point, string key = null)
        {
            return CreteInitial(point, $"TenantInitial{key}");
        }

        public ContractMonthToLease() : base($"MonthToLease\\{nameof(ContractMonthToLease)}.pdf", 6, 8.25f)
        {
            var page = Pages[1];

            #region page1

            page.Add(CreteTenantInitial(new Point(80, 771)));
            page.Add(CreteTenantInitial(new Point(114, 771), "1"));
            page.Add(CreteTenantInitial(new Point(148, 771), "2"));
            page.Add(CreteTenantInitial(new Point(182, 771), "3"));

            page.Add(CreteLordInitial(new Point(393, 771)));
            page.Add(CreteLordInitial(new Point(428, 771), "1"));

            page.Add(new WordToReplace("This", new[] {new Point(472, 129)}));
            page.Add(new WordToReplace("DayOf", new[] {new Point(35, 140)}));
            page.Add(new WordToReplace("Year", new[] {new Point(187, 140)}));
            page.Add(new WordToReplace("Between", new[] {new Point(280, 140)}));
            page.Add(new WordToReplace("LandlordAddress", new[] {new Point(105, 152)}));
            page.Add(new WordToReplace("TenantAddress", new[] {new Point(53, 163)}));
            page.Add(new WordToReplace("County", new[] {new Point(415, 208)}));
            page.Add(new WordToReplace("StreetAddress", new[] {new Point(250, 219)}));
            page.Add(new WordToReplace("Term", new[] {new Point(232, 331)}));
            page.Add(new WordToReplace("TermA", new[] {new Point(53, 353)}));
            page.Add(new WordToReplace("TermB", new[] {new Point(53, 431)}));
            page.Add(new WordToReplace("TermDate", new[] {new Point(470, 432)}));
            page.Add(new WordToReplace("RentLandlord", new[] {new Point(53, 554)}));
            page.Add(new WordToReplace("RentDollars", new[] {new Point(492, 554)}));
            page.Add(new WordToReplace("RentPersonal", new[] {new Point(376, 621)}));
            page.Add(new WordToReplace("RentMoney", new[] {new Point(463, 621)}));
            page.Add(new WordToReplace("RentCashier", new[] {new Point(539, 621)}));
            page.Add(new WordToReplace("RentOr", new[] {new Point(128, 633)}));
            page.Add(new WordToReplace("RentOther", new[] {new Point(180, 633)}));
            page.Add(new WordToReplace("RentAddress", new[] {new Point(170, 644)}));
            page.Add(new WordToReplace("RentAddress1", new[] {new Point(53, 655)}));
            page.Add(new WordToReplace("SecuritySum", new[] {new Point(53, 734)}));
            page.Add(new WordToReplace("SecurityDollars", new[] {new Point(472, 734)}));

            #endregion

            page = Pages[2];

            #region page2

            page.Add(CreteTenantInitial(new Point(80, 771)));
            page.Add(CreteTenantInitial(new Point(114, 771), "1"));
            page.Add(CreteTenantInitial(new Point(148, 771),"2"));
            page.Add(CreteTenantInitial(new Point(182, 771), "3"));

            page.Add(CreteLordInitial(new Point(393, 771)));
            page.Add(CreteLordInitial(new Point(428, 771), "1"));

            page.Add(new WordToReplace("SecurityInstitution", new[] {new Point(455, 125)}));
            page.Add(new WordToReplace("SecurityInstitution1", new[] {new Point(53, 136)}));
            page.Add(new WordToReplace("SecurityAmount", new[] {new Point(60, 271)}));
            page.Add(new WordToReplace("SecurityDue", new[] {new Point(283, 271)}));
            page.Add(new WordToReplace("PremisesConsisting", new[] {new Point(53, 517)}));

            #endregion


            page = Pages[3];

            #region page3

            page.Add(CreteTenantInitial(new Point(81, 771)));
            page.Add(CreteTenantInitial(new Point(114, 771), "1"));
            page.Add(CreteTenantInitial(new Point(148, 771),"2"));
            page.Add(CreteTenantInitial(new Point(182, 771), "3"));

            page.Add(CreteLordInitial(new Point(393, 771)));
            page.Add(CreteLordInitial(new Point(428, 771), "1"));

            #endregion


            page = Pages[4];

            #region page4

            page.Add(CreteTenantInitial(new Point(81, 771)));
            page.Add(CreteTenantInitial(new Point(114, 771), "1"));
            page.Add(CreteTenantInitial(new Point(148, 771), "2"));
            page.Add(CreteTenantInitial(new Point(182, 771), "3"));

            page.Add(CreteLordInitial(new Point(393, 771)));
            page.Add(CreteLordInitial(new Point(428, 771), "1"));

            page.Add(new WordToReplace("TenantsOwing", new[] {new Point(430, 215)}));
            page.Add(new WordToReplace("TenantsOwing1", new[] {new Point(55, 226)}));
            page.Add(new WordToReplace("TenantsDollars", new[] {new Point(381, 226)}));
            page.Add(new WordToReplace("AnimalsMore", new[] {new Point(286, 304)}));
            page.Add(new WordToReplace("AnimalsMore1", new[] {new Point(350, 304)}));
            page.Add(new WordToReplace("AnimalsDeposit", new[] {new Point(53, 326)}));
            page.Add(new WordToReplace("AnimalsDollars", new[] {new Point(488, 327)}));
            page.Add(new WordToReplace("AnimalsDeposit1", new[] {new Point(53, 338)}));
            page.Add(new WordToReplace("AnimalsDollars1", new[] {new Point(480, 338)}));

            #endregion

            page = Pages[5];

            #region page5

            page.Add(CreteTenantInitial(new Point(80, 771)));
            page.Add(CreteTenantInitial(new Point(114, 771), "1"));
            page.Add(CreteTenantInitial(new Point(148, 771), "2"));
            page.Add(CreteTenantInitial(new Point(182, 771), "3"));

            page.Add(CreteLordInitial(new Point(393, 771)));
            page.Add(CreteLordInitial(new Point(428, 771), "1"));

            page.Add(new WordToReplace("ChargeAmount", new[] {new Point(53, 114)}));
            page.Add(new WordToReplace("ChargeDollars", new[] {new Point(478, 114)}));

            #endregion

            page = Pages[6];

            #region page6

            page.Add(CreteTenantInitial(new Point(80, 771)));
            page.Add(CreteTenantInitial(new Point(114, 771), "1"));
            page.Add(CreteTenantInitial(new Point(148, 771), "2"));
            page.Add(CreteTenantInitial(new Point(182, 771), "3"));

            page.Add(CreteLordInitial(new Point(393, 771)));
            page.Add(CreteLordInitial(new Point(428, 771), "1"));

            page.Add(new WordToReplace("NoticeLandlord", new[] {new Point(55, 114)}));
            page.Add(new WordToReplace("NoticeTenant", new[] {new Point(297, 114)}));
            page.Add(new WordToReplace("NoticeLandlord1", new[] {new Point(54, 143)}));
            page.Add(new WordToReplace("NoticeTenant1", new[] {new Point(297, 143)}));
            page.Add(new WordToReplace("NoticeLandlordAddress", new[] {new Point(54, 166)}));
            page.Add(new WordToReplace("NoticeTenantAddress", new[] {new Point(297, 165)}));
            page.Add(new WordToReplace("Additional", new[] {new Point(260, 239)}));
            page.Add(new WordToReplace("Additional1", new[] {new Point(53, 256)}));
            page.Add(new WordToReplace("Additional2", new[] {new Point(53, 273)}));
            page.Add(new WordToReplace("Additional3", new[] {new Point(53, 289)}));
            page.Add(new WordToReplace("Additional4", new[] {new Point(53, 306)}));
            page.Add(new WordToReplace("AdditionalLandlordSign", new[] {new Point(58, 419)}));
            page.Add(new WordToReplace("AdditionalLandlordPrint", new[] {new Point(322, 419)}));
            page.Add(new WordToReplace("AdditionalLandlordSign1", new[] {new Point(58, 475)}));
            page.Add(new WordToReplace("AdditionalLandlordPrint1", new[] {new Point(322, 475)}));
            page.Add(new WordToReplace("AdditionalTenantSign", new[] {new Point(60, 554)}));
            page.Add(new WordToReplace("AdditionalTenantPrint", new[] {new Point(323, 554)}));
            page.Add(new WordToReplace("AdditionalTenantSign1", new[] {new Point(60, 610)}));
            page.Add(new WordToReplace("AdditionalTenantPrint1", new[] {new Point(323, 610)}));
            page.Add(new WordToReplace("AdditionalTenantSign2", new[] {new Point(58, 666)}));
            page.Add(new WordToReplace("AdditionalTenantPrint2", new[] {new Point(322, 666)}));
            page.Add(new WordToReplace("AdditionalTenantSign3", new[] {new Point(58, 722)}));
            page.Add(new WordToReplace("AdditionalTenantPrint3", new[] {new Point(322, 722)}));

            #endregion
        }
    }
}