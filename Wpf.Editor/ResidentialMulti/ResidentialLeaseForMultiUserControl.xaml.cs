﻿using System.Windows;
using System.Windows.Data;

namespace Wpf.Editor.ResidentialMulti
{ 
    public partial class ResidentialLeaseForMultiUserControl
    {
        public string LordInitial
        {
            get => (string)GetValue(LordInitialProperty);
            set => SetValue(LordInitialProperty, value);
        }

        public static readonly DependencyProperty LordInitialProperty = DependencyProperty.Register(
            nameof(LordInitial),
            typeof(string),
            typeof(ResidentialLeaseForMultiUserControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
                { DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged }); 

        public string LordInitial1
        {
            get => (string)GetValue(LordInitial1Property);
            set => SetValue(LordInitial1Property, value);
        }

        public static readonly DependencyProperty LordInitial1Property = DependencyProperty.Register(
            nameof(LordInitial1),
            typeof(string),
            typeof(ResidentialLeaseForMultiUserControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
                { DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });


        public string TenantInitial
        {
            get => (string)GetValue(TenantInitialProperty);
            set => SetValue(TenantInitialProperty, value);
        }

        public static readonly DependencyProperty TenantInitialProperty = DependencyProperty.Register(
            nameof(TenantInitial),
            typeof(string),
            typeof(ResidentialLeaseForMultiUserControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
                { DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged }); 

        public string TenantInitial1
        {
            get => (string)GetValue(TenantInitial1Property);
            set => SetValue(TenantInitial1Property, value);
        }

        public static readonly DependencyProperty TenantInitial1Property = DependencyProperty.Register(
            nameof(TenantInitial1),
            typeof(string),
            typeof(ResidentialLeaseForMultiUserControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
                { DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

        public ResidentialLeaseForMultiUserControl()
        {
            InitializeComponent();
        }
    }
}
