﻿using System.Windows;

namespace Wpf.Editor.ResidentialMulti
{
    public class ResidentialLeaseForMulti : Contract
    {
        protected WordToReplace CreteLordInitial(Point point, string key = null)
        {
            return CreteInitial(point, $"LordInitial{key}");
        }

        private WordToReplace CreteTenantInitial(Point point, string key = null)
        {
            return CreteInitial(point, $"TenantInitial{key}");
        }

        public ResidentialLeaseForMulti()
            : base($"ResidentialMulti\\{nameof(ResidentialLeaseForMulti)}.pdf", 8, 10)
        {
            var page = Pages[1];

            #region page1

            page.Add(new WordToReplace("Name", new[] {new Point(85, 283),}));
            page.Add(new WordToReplace("Name1", new[] {new Point(100, 385),}));
            page.Add(new WordToReplace("Name2", new[] {new Point(85, 436),}));
            page.Add(new WordToReplace("Name3", new[] {new Point(370, 460),}));
            page.Add(new WordToReplace("Name4", new[] {new Point(195, 496),}));


            page.Add(new WordToReplace("LordOrTenant", new[] {new Point(48, 587),}));
            page.Add(new WordToReplace("LordOrTenant1", new[] {new Point(48, 607),}));
            page.Add(new WordToReplace("LordOrTenantName", new[] {new Point(348, 607),}));
            page.Add(new WordToReplace("LordOrTenantLanguage", new[] {new Point(60, 630),}));

            page.Add(new WordToReplace("LordOrTenantSignature", new[] {new Point(358, 665),}));
            page.Add(new WordToReplace("LicenseeSignature", new[] {new Point(52, 711),}));
            page.Add(new WordToReplace("LordOrTenantSignature1", new[] {new Point(358, 711),}));

            #endregion

            page = Pages[2];

            #region page2 
            page.Add(CreteTenantInitial(new Point(69, 731)));
            page.Add(CreteTenantInitial(new Point(105, 731), "1"));
            page.Add(CreteLordInitial(new Point(199, 731)));
            page.Add(CreteLordInitial(new Point(234, 731), "1"));


            page.Add(new WordToReplace("BlankSpace", new[] {new Point(197, 159),}));
            page.Add(new WordToReplace("Beginning", new[] {new Point(210, 220),}));
            page.Add(new WordToReplace("Ending", new[] {new Point(370, 220),}));
            page.Add(new WordToReplace("NameOfOwner", new[] {new Point(35, 261),}));
            page.Add(new WordToReplace("NameOfPerson", new[] {new Point(35, 289),}));
            page.Add(new WordToReplace("LandlordAddress", new[] {new Point(200, 343),}));
            page.Add(new WordToReplace("LandlordTelephone", new[] {new Point(200, 355),}));
            page.Add(new WordToReplace("TenantAddress", new[] {new Point(200, 367),}));
            page.Add(new WordToReplace("TenantTelephone", new[] {new Point(200, 379),}));
            page.Add(new WordToReplace("PropertyRented", new[] {new Point(385, 397),}));
            page.Add(new WordToReplace("StreetAddress", new[] {new Point(35, 416),}));
            page.Add(new WordToReplace("NameOfApartment", new[] {new Point(35, 449),}));
            page.Add(new WordToReplace("PropertyCity", new[] {new Point(395, 449),}));
            page.Add(new WordToReplace("ZipCode", new[] {new Point(85, 477),}));
            page.Add(new WordToReplace("PropertyAppliances", new[] {new Point(35, 500),}));
            page.Add(new WordToReplace("PropertyAppliances1", new[] {new Point(35, 522),}));
            page.Add(new WordToReplace("PropertyAppliances2", new[] {new Point(35, 545),}));
            page.Add(new WordToReplace("RentPayments", new[] {new Point(472, 628),}));
            page.Add(new WordToReplace("RentPayments1", new[] {new Point(48, 645),}));
            page.Add(new WordToReplace("RentPayments2", new[] {new Point(223, 645),}));
            page.Add(new WordToReplace("RentPayments3", new[] {new Point(120, 680),}));
            page.Add(new WordToReplace("RentPayments4", new[] {new Point(142, 693),}));

            #endregion

            page = Pages[3];

            #region page3

            page.Add(CreteTenantInitial(new Point(67, 732)));
            page.Add(CreteTenantInitial(new Point(104, 732), "1"));
            page.Add(CreteLordInitial(new Point(197, 732)));
            page.Add(CreteLordInitial(new Point(232, 732), "1"));

            page.Add(new WordToReplace("RentPayments5", new[] {new Point(100, 42),}));
            page.Add(new WordToReplace("RentPaymentsDate", new[] {new Point(390, 43),}));

            page.Add(new WordToReplace("RentPaymentsDate1", new[] {new Point(33, 84),}));
            page.Add(new WordToReplace("RentPaymentsDate2", new[] {new Point(213, 84),}));
            page.Add(new WordToReplace("RentPaymentsAmount", new[] {new Point(433, 84),}));

            page.Add(new WordToReplace("RentPaymentsDate3", new[] {new Point(47, 116),}));

            page.Add(new WordToReplace("SecurityDeposit", new[] {new Point(54, 178),}));
            page.Add(new WordToReplace("SecurityDepositAmount", new[] {new Point(192, 177),}));

            page.Add(new WordToReplace("AdvanceRentAmount", new[] {new Point(232, 198),}));
            page.Add(new WordToReplace("DepositInstallmentPeriods", new[] {new Point(470, 198),}));
            page.Add(new WordToReplace("AdvanceRent", new[] {new Point(54, 209),}));

            page.Add(new WordToReplace("PetDeposit", new[] {new Point(54, 229),}));

            page.Add(new WordToReplace("PetDepositAmount", new[] {new Point(230, 229),}));

            page.Add(new WordToReplace("LateChargeAmount", new[] {new Point(238, 249),}));
            page.Add(new WordToReplace("LeasePaymentMode", new[] {new Point(505, 249),}));

            page.Add(new WordToReplace("LateCharge", new[] {new Point(54, 260),}));

            page.Add(new WordToReplace("BadCheckAmount", new[] {new Point(252, 280),}));
            page.Add(new WordToReplace("BadCheck", new[] {new Point(54, 301),}));

            page.Add(new WordToReplace("DepositOther", new[] {new Point(54, 322),}));
            page.Add(new WordToReplace("DepositOther1", new[] {new Point(125, 322),}));
            page.Add(new WordToReplace("DepositOther2", new[] {new Point(54, 344),}));
            page.Add(new WordToReplace("DepositOther3", new[] {new Point(125, 344),}));

            page.Add(new WordToReplace("NoticesName", new[] {new Point(95, 516),}));
            page.Add(new WordToReplace("NoticesLordAgentAddress", new[] {new Point(245, 540),}));

            #endregion

            page = Pages[4];

            #region page4
            page.Add(CreteTenantInitial(new Point(73, 731)));
            page.Add(CreteTenantInitial(new Point(110, 731), "1"));
            page.Add(CreteLordInitial(new Point(203, 731)));
            page.Add(CreteLordInitial(new Point(239, 731), "1"));

            page.Add(new WordToReplace("UseOfPremises", new[] {new Point(109, 43)}));
            page.Add(new WordToReplace("UseOfPremises1", new[] {new Point(485, 85)}));
            page.Add(new WordToReplace("UseOfPremises2", new[] {new Point(109, 116)}));
            page.Add(new WordToReplace("UseOfPremises3", new[] {new Point(109, 147)}));
            page.Add(new WordToReplace("UseOfPremises4", new[] {new Point(262, 262)}));

            page.Add(new WordToReplace("LordSmoke", new[] {new Point(43, 489)}));
            page.Add(new WordToReplace("TenantSmoke", new[] {new Point(94, 489)}));
            page.Add(new WordToReplace("LordExtermination", new[] {new Point(43, 500)}));
            page.Add(new WordToReplace("TenantExtermination", new[] {new Point(94, 500)}));

            page.Add(new WordToReplace("LordLocks", new[] {new Point(43, 510)}));
            page.Add(new WordToReplace("TenantLocks", new[] {new Point(94, 510)}));

            page.Add(new WordToReplace("LordClean", new[] {new Point(43, 521)}));
            page.Add(new WordToReplace("TenantClean", new[] {new Point(94, 521)}));

            page.Add(new WordToReplace("LordGarbage", new[] {new Point(43, 531)}));
            page.Add(new WordToReplace("TenantGarbage", new[] {new Point(94, 531)}));

            page.Add(new WordToReplace("LordRunning", new[] {new Point(43, 542)}));
            page.Add(new WordToReplace("TenantRunning", new[] {new Point(94, 542)}));

            page.Add(new WordToReplace("LordWater", new[] {new Point(43, 552)}));
            page.Add(new WordToReplace("TenantWater", new[] {new Point(94, 552)}));

            page.Add(new WordToReplace("LordLawn", new[] {new Point(43, 562)}));
            page.Add(new WordToReplace("TenantLawn", new[] {new Point(94, 562)}));

            page.Add(new WordToReplace("LordHeat", new[] {new Point(43, 572)}));
            page.Add(new WordToReplace("TenantHEat", new[] {new Point(94, 572)}));

            page.Add(new WordToReplace("LordAir", new[] {new Point(43, 582)}));
            page.Add(new WordToReplace("TenantAir", new[] {new Point(94, 582)}));

            page.Add(new WordToReplace("LordFurniture", new[] {new Point(43, 593)}));
            page.Add(new WordToReplace("TenantFurniture", new[] {new Point(94, 593)}));

            page.Add(new WordToReplace("LordAppliances", new[] {new Point(43, 603)}));
            page.Add(new WordToReplace("TenantAppliances", new[] {new Point(94, 603)}));

            page.Add(new WordToReplace("LordFixtures", new[] {new Point(43, 614)}));
            page.Add(new WordToReplace("TenantFixtures", new[] {new Point(94, 614)}));

            page.Add(new WordToReplace("LordPool", new[] {new Point(43, 625)}));
            page.Add(new WordToReplace("TenantPool", new[] {new Point(94, 625)}));

            page.Add(new WordToReplace("LordHeating", new[] {new Point(43, 635)}));
            page.Add(new WordToReplace("TenantHeating", new[] {new Point(94, 635)}));

            page.Add(new WordToReplace("LordOther", new[] {new Point(43, 645)}));
            page.Add(new WordToReplace("TenantOther", new[] {new Point(94, 645)}));
            page.Add(new WordToReplace("MaintenanceOther", new[] {new Point(175, 645)}));

            page.Add(new WordToReplace("MaintenanceCostsMore", new[] {new Point(420, 710)}));

            #endregion

            page = Pages[5];

            #region page5
            page.Add(CreteTenantInitial(new Point(73, 730)));
            page.Add(CreteTenantInitial(new Point(110, 730), "1"));
            page.Add(CreteLordInitial(new Point(203, 730)));
            page.Add(CreteLordInitial(new Point(239, 730), "1"));

            page.Add(new WordToReplace("Utilities", new[] {new Point(225,199)}));
            page.Add(new WordToReplace("Assignment", new[] {new Point(286,571)}));

            #endregion

            page = Pages[6];

            #region page6
            page.Add(CreteTenantInitial(new Point(73, 733)));
            page.Add(CreteTenantInitial(new Point(110, 733), "1"));
            page.Add(CreteLordInitial(new Point(203, 733)));
            page.Add(CreteLordInitial(new Point(239, 733), "1"));

            page.Add(new WordToReplace("Approval", new[] { new Point(346,55) }));
            page.Add(new WordToReplace("Approval1", new[] { new Point(404,55) }));
            page.Add(new WordToReplace("Approval2", new[] { new Point(425,97) }));
            page.Add(new WordToReplace("Approval3", new[] { new Point(481,97) }));

            page.Add(new WordToReplace("LeadBasedPaint", new[] { new Point(162,157) }));

            page.Add(new WordToReplace("LeadBasedPaintAInitial", new[] { new Point(40,264) }));
            page.Add(new WordToReplace("LeadBasedPaintAI", new[] { new Point(131,275) }));
            page.Add(new WordToReplace("LeadBasedPaintAI1", new[] { new Point(40,295) }));
            page.Add(new WordToReplace("LeadBasedPaintAI2", new[] { new Point(40,316) })); 
            page.Add(new WordToReplace("LeadBasedPaintAII", new[] { new Point(131,337) }));
            page.Add(new WordToReplace("LeadBasedPaintBInitial", new[] { new Point(40,346) }));
            page.Add(new WordToReplace("LeadBasedPaintBI", new[] { new Point(131,359) })); 
            page.Add(new WordToReplace("LeadBasedPaintBI1", new[] { new Point(40,388) }));
            page.Add(new WordToReplace("LeadBasedPaintBI2", new[] { new Point(40,409) })); 
            page.Add(new WordToReplace("LeadBasedPaintBII", new[] { new Point(131,429) }));


            page.Add(new WordToReplace("LeadBasedPaintCInitial", new[] { new Point(40, 481) }));
            page.Add(new WordToReplace("LeadBasedPaintDInitial", new[] { new Point(40,492) }));
                                                      
            page.Add(new WordToReplace("LeadBasedPaintEInitial", new[] { new Point(40,534) }));

            page.Add(new WordToReplace("LessorsSignature", new[] { new Point(40,634) }));
            page.Add(new WordToReplace("Date", new[] { new Point(220,634) }));
            page.Add(new WordToReplace("LessorsSignature1", new[] { new Point(320,634) })); 
            page.Add(new WordToReplace("Date1", new[] { new Point(495,634) }));

            page.Add(new WordToReplace("LessorsSignature2", new[] { new Point(40,665) }));
            page.Add(new WordToReplace("Date2", new[] { new Point(220,665) })); 
            page.Add(new WordToReplace("LessorsSignature3", new[] { new Point(320, 665) }));
            page.Add(new WordToReplace("Date3", new[] { new Point(495, 665) }));

            page.Add(new WordToReplace("AgentsSignature", new[] { new Point(40, 696) })); 
            page.Add(new WordToReplace("Date4", new[] { new Point(220, 696) }));
            page.Add(new WordToReplace("AgentsSignature1", new[] { new Point(320, 696) }));
            page.Add(new WordToReplace("Date5", new[] { new Point(495, 696) }));
            #endregion

            page = Pages[7];

            #region page7
            page.Add(CreteTenantInitial(new Point(73, 732)));
            page.Add(CreteTenantInitial(new Point(110, 732), "1"));
            page.Add(CreteLordInitial(new Point(203, 732)));
            page.Add(CreteLordInitial(new Point(239, 732), "1"));

            page.Add(new WordToReplace("TenantsPersonalProperty", new[] { new Point(383,265) }));

            page.Add(new WordToReplace("LandlordSignature", new[] { new Point(40,358) }));
            page.Add(new WordToReplace("LandLordDate", new[] { new Point(335,358) }));

            page.Add(new WordToReplace("LandlordSignature1", new[] { new Point(40,388) }));
            page.Add(new WordToReplace("LandLordDate1", new[] { new Point(335,389) }));

            page.Add(new WordToReplace("LandlordSignature2", new[] { new Point(40,419) }));
            page.Add(new WordToReplace("LandLordDate2", new[] { new Point(335,419) }));

            page.Add(new WordToReplace("LandlordSignature3", new[] { new Point(40,451) }));
            page.Add(new WordToReplace("LandLordDate3", new[] { new Point(335,451) }));

            page.Add(new WordToReplace("NameIndividual", new[] { new Point(150,532) }));
            page.Add(new WordToReplace("NameBusiness", new[] { new Point(150,544) }));
            page.Add(new WordToReplace("Address", new[] { new Point(150,556) }));
            page.Add(new WordToReplace("TelephoneNumber", new[] { new Point(150,568) }));

            #endregion

            page = Pages[8];

            #region page8
            page.Add(CreteTenantInitial(new Point(73, 731)));
            page.Add(CreteTenantInitial(new Point(110, 731), "1"));
            page.Add(CreteLordInitial(new Point(203, 731)));
            page.Add(CreteLordInitial(new Point(239, 731), "1"));

            page.Add(new WordToReplace("Agree", new[] { new Point(48,69) }));
            page.Add(new WordToReplace("Pay", new[] { new Point(300,68) }));

            page.Add(new WordToReplace("NotAgree", new[] { new Point(47,108) }));

            page.Add(new WordToReplace("EarlyLandLordSignature", new[] { new Point(40,193) }));
            page.Add(new WordToReplace("EarlyLandLordDate", new[] { new Point(330,193) }));

            page.Add(new WordToReplace("EarlyLandLordSignature1", new[] { new Point(40,245) }));
            page.Add(new WordToReplace("EarlyLandLordDate1", new[] { new Point(330,245) }));

            page.Add(new WordToReplace("EarlyLandLordSignature2", new[] { new Point(40,298) }));
            page.Add(new WordToReplace("EarlyLandLordDate2", new[] { new Point(330,298) }));

            page.Add(new WordToReplace("EarlyLandLordSignature3", new[] { new Point(40,350) }));
            page.Add(new WordToReplace("EarlyLandLordDate3", new[] { new Point(330,350) })); 


            #endregion
        }
    }
}