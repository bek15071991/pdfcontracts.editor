﻿using System;
using System.Collections.Generic;
using System.Windows;
using JetBrains.Annotations;

namespace Wpf.Editor
{
    public enum ContractType
    {
        ContractToLease = 1,
        MonthLease = 2,
        AsIsResidential = 3,
        ResidentialLeaseForMulti = 4,
        ResidentialLeaseForSingle = 5
    }

    public abstract class Contract
    {
        public string PdfPath { get; protected set; }

        public int PageCount { get; protected set; }

        public float FontSize { get; protected set; }

        public IDictionary<int, IList<WordToReplace>> Pages { get; } = new Dictionary<int, IList<WordToReplace>>();

        protected Contract([NotNull] string pdfPath, int pageCount, float fontSize)
        {
            PdfPath = pdfPath ?? throw new ArgumentNullException(nameof(pdfPath));
            PageCount = pageCount;
            FontSize = fontSize;

            for (var i = 1; i <= pageCount; i++)
                Pages[i] = new List<WordToReplace>();
        }

        protected WordToReplace CreteInitial(Point point, [NotNull] string key)
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));

            return new WordToReplace(key, new[] { point });
        }  
    }
}