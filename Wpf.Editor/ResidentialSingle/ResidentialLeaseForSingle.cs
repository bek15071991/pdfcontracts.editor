﻿using System.Windows;

namespace Wpf.Editor.ResidentialSingle
{
    public class ResidentialLeaseForSingle : Contract
    {
        protected WordToReplace CreteLordInitial(Point point, string key = null)
        {
            return CreteInitial(point, $"LordInitial{key}");
        }

        private WordToReplace CreteTenantInitial(Point point, string key = null)
        {
            return CreteInitial(point, $"TenantInitial{key}");
        } 

        public ResidentialLeaseForSingle()
            : base($"ResidentialSingle\\{nameof(ResidentialLeaseForSingle)}.pdf", 19, 9.25f)
        {
            var page = Pages[1];

            #region page1

            page.Add(new WordToReplace("Name", new[] {new Point(85, 283),}));
            page.Add(new WordToReplace("Name1", new[] {new Point(100, 385),}));
            page.Add(new WordToReplace("Name2", new[] {new Point(85, 436),}));
            page.Add(new WordToReplace("Name3", new[] {new Point(370, 460),}));
            page.Add(new WordToReplace("Name4", new[] {new Point(195, 496),}));
            page.Add(new WordToReplace("LordOrTenant", new[] {new Point(48, 587),}));
            page.Add(new WordToReplace("LordOrTenant1", new[] {new Point(48, 607),}));
            page.Add(new WordToReplace("LordOrTenantName", new[] {new Point(348, 607),}));
            page.Add(new WordToReplace("LordOrTenantLanguage", new[] {new Point(60, 630),}));

            page.Add(new WordToReplace("LordOrTenantSignature", new[] {new Point(358, 665),}));
            page.Add(new WordToReplace("LicenseeSignature", new[] {new Point(52, 711),}));
            page.Add(new WordToReplace("LordOrTenantSignature1", new[] {new Point(358, 711),}));

            #endregion

            page = Pages[2];

            #region page2

            page.Add(CreteTenantInitial(new Point(70, 734)));
            page.Add(CreteTenantInitial(new Point(106, 734), "1"));
            page.Add(CreteLordInitial(new Point(200, 734)));
            page.Add(CreteLordInitial(new Point(235, 734), "1"));

            page.Add(new WordToReplace("Box", new[] {new Point(69, 83),}));
            page.Add(new WordToReplace("BlankSpace", new[] {new Point(193, 83),}));
            page.Add(new WordToReplace("LordNameAddress", new[] {new Point(245, 155),}));
            page.Add(new WordToReplace("LordNameAddress1", new[] {new Point(40, 186),}));
            page.Add(new WordToReplace("TenantNameAddress", new[] {new Point(40, 205),}));
            page.Add(new WordToReplace("TenantNameAddress1", new[] {new Point(40, 236),}));
            page.Add(new WordToReplace("LandlordAddress", new[] {new Point(205, 257),}));
            page.Add(new WordToReplace("LandlordTelephone", new[] {new Point(205, 278),}));
            page.Add(new WordToReplace("TenantAddress", new[] {new Point(205, 298),}));
            page.Add(new WordToReplace("TenantTelephone", new[] {new Point(205, 319),}));
            page.Add(new WordToReplace("RentedStreetAddress", new[] {new Point(380, 340),}));
            page.Add(new WordToReplace("RentedStreetAddress1", new[] {new Point(40, 370),}));
            page.Add(new WordToReplace("RentedZipCode", new[] {new Point(500, 370),}));
            page.Add(new WordToReplace("Rented", new[] {new Point(40, 432),}));
            page.Add(new WordToReplace("Rented1", new[] {new Point(40, 455),}));
            page.Add(new WordToReplace("RentedPersons", new[] {new Point(353, 487),}));
            page.Add(new WordToReplace("RentedPersons1", new[] {new Point(40, 508),}));

            page.Add(new WordToReplace("TermBeginning", new[] {new Point(415, 528),}));
            page.Add(new WordToReplace("TermEnding", new[] {new Point(75, 549),}));
            page.Add(new WordToReplace("RentAmountTaxes", new[] {new Point(421, 573),}));
            page.Add(new WordToReplace("RentInstallments", new[] {new Point(38, 608),}));
            page.Add(new WordToReplace("RentMonthly", new[] {new Point(74, 629),}));
            page.Add(new WordToReplace("RentMonthlyDay", new[] {new Point(160, 630),}));
            page.Add(new WordToReplace("RentMonthlyAmount", new[] {new Point(80, 640),}));

            page.Add(new WordToReplace("RentWeekly", new[] {new Point(74, 680),}));
            page.Add(new WordToReplace("RentWeeklyDay", new[] {new Point(150, 682),}));
            page.Add(new WordToReplace("RentWeeklyAmount", new[] {new Point(520, 682),}));

            page.Add(new WordToReplace("RentFull", new[] {new Point(38, 711),}));
            page.Add(new WordToReplace("RentFullDate", new[] {new Point(90, 712),}));
            page.Add(new WordToReplace("RentFullAmount", new[] {new Point(303, 712)}));

            #endregion

            page = Pages[3];

            #region page3

            page.Add(CreteTenantInitial(new Point(70, 733)));
            page.Add(CreteTenantInitial(new Point(106, 733), "1"));
            page.Add(CreteLordInitial(new Point(200, 733)));
            page.Add(CreteLordInitial(new Point(235, 733), "1"));

            page.Add(new WordToReplace("RentAmount", new[] {new Point(395, 44),}));
            page.Add(new WordToReplace("Rent", new[] {new Point(461, 44),}));
            page.Add(new WordToReplace("Rent1", new[] {new Point(38, 66),}));
            page.Add(new WordToReplace("RentPaymentSummary", new[] {new Point(38, 107),}));
            page.Add(new WordToReplace("RentPaymentSummary1", new[] {new Point(502, 106),}));
            page.Add(new WordToReplace("RentPaymentSummary2", new[] {new Point(38, 127),}));
            page.Add(new WordToReplace("RentPaymentSummary3", new[] {new Point(400, 126),}));
            page.Add(new WordToReplace("RentPaymentSummaryName", new[] {new Point(190, 148),}));
            page.Add(new WordToReplace("RentPaymentSummaryAddress", new[] {new Point(40, 167),}));
            page.Add(new WordToReplace("RentPaymentSummaryTenancy", new[] {new Point(38, 188),}));
            page.Add(new WordToReplace("RentPaymentSummaryFromDate", new[] {new Point(40, 208),}));
            page.Add(new WordToReplace("RentPaymentSummaryThroughDate", new[] {new Point(220, 208),}));
            page.Add(new WordToReplace("RentPaymentSummaryAmount", new[] {new Point(445, 208),}));
            page.Add(new WordToReplace("RentPaymentSummaryOnDate", new[] {new Point(55, 240),}));

            page.Add(new WordToReplace("RentPaymentSummaryCash", new[] {new Point(387, 271),}));
            page.Add(new WordToReplace("RentPaymentSummaryPersonalCheck", new[] {new Point(430, 270),}));
            page.Add(new WordToReplace("RentPaymentSummaryMoneyOrder", new[] {new Point(515, 270),}));
            page.Add(new WordToReplace("RentPaymentSummaryCashierCheck", new[] {new Point(39, 281),}));
            page.Add(new WordToReplace("RentPaymentSummaryOr", new[] {new Point(143, 281),}));
            page.Add(new WordToReplace("RentPaymentSummaryOther", new[] {new Point(185, 281),}));
            page.Add(new WordToReplace("RentPaymentSummaryRequireTenant", new[] {new Point(392, 312),}));
            page.Add(new WordToReplace("RentPaymentSummaryMoney", new[] {new Point(535, 312),}));
            page.Add(new WordToReplace("RentPaymentSummaryTenantCash", new[] {new Point(240, 321),}));
            page.Add(new WordToReplace("RentPaymentSummaryTenantOther", new[] {new Point(355, 322),}));
            page.Add(new WordToReplace("RentPaymentSummaryTenantPayBad", new[] {new Point(60, 332),}));
            page.Add(new WordToReplace("RentPaymentSummaryTenantAmount", new[] {new Point(252, 333),}));
            page.Add(new WordToReplace("Money", new[] {new Point(345, 364),}));
            page.Add(new WordToReplace("MoneyToName", new[] {new Point(52, 415),}));
            page.Add(new WordToReplace("MoneyAtAddress", new[] {new Point(52, 445),}));

            page.Add(new WordToReplace("MoneyMonth", new[] {new Point(58, 475),}));
            page.Add(new WordToReplace("MoneyWeek", new[] {new Point(108, 475),}));
            page.Add(new WordToReplace("MoneyTaxes", new[] {new Point(273, 475),}));
            page.Add(new WordToReplace("MoneyTaxesDue", new[] {new Point(395, 475),}));

            page.Add(new WordToReplace("MoneyTaxes1", new[] {new Point(273, 496),}));
            page.Add(new WordToReplace("MoneyTaxesDue1", new[] {new Point(395, 496),}));

            page.Add(new WordToReplace("MoneyAdvanceMonth", new[] {new Point(107, 517),}));
            page.Add(new WordToReplace("MoneyAdvanceWeek", new[] {new Point(152, 517),}));

            page.Add(new WordToReplace("MoneyAdvanceWeek1", new[] {new Point(40, 538),}));
            page.Add(new WordToReplace("MoneyAdvanceTaxes", new[] {new Point(273, 538),}));
            page.Add(new WordToReplace("MoneyAdvanceDue", new[] {new Point(395, 538),}));

            page.Add(new WordToReplace("MoneyAdvanceLastMonth", new[] {new Point(60, 558),}));
            page.Add(new WordToReplace("MoneyAdvanceLastWeek", new[] {new Point(107, 557),}));
            page.Add(new WordToReplace("MoneyAdvanceLastTaxes", new[] {new Point(273, 558),}));
            page.Add(new WordToReplace("MoneyAdvanceLastDue", new[] {new Point(395, 558),}));

            page.Add(new WordToReplace("MoneySecurityTaxes", new[] {new Point(273, 579),}));
            page.Add(new WordToReplace("MoneySecurityDue", new[] {new Point(395, 579),}));

            page.Add(new WordToReplace("MoneyAdditionalTaxes", new[] {new Point(273, 600),}));
            page.Add(new WordToReplace("MoneyAdditionalDue", new[] {new Point(395, 600),}));

            page.Add(new WordToReplace("MoneyHomeownerTaxes", new[] {new Point(273, 620),}));
            page.Add(new WordToReplace("MoneyHomeownerDue", new[] {new Point(395, 620),}));

            page.Add(new WordToReplace("MoneyPetTaxes", new[] {new Point(273, 641),}));
            page.Add(new WordToReplace("MoneyPetDue", new[] {new Point(395, 641),}));

            page.Add(new WordToReplace("MoneyOther", new[] {new Point(63, 662),}));
            page.Add(new WordToReplace("MoneyOtherTaxes", new[] {new Point(273, 663),}));
            page.Add(new WordToReplace("MoneyOtherDue", new[] {new Point(395, 664),}));

            page.Add(new WordToReplace("MoneyOther1", new[] {new Point(63, 682),}));
            page.Add(new WordToReplace("MoneyOtherTaxes1", new[] {new Point(273, 682),}));
            page.Add(new WordToReplace("MoneyOtherDue1", new[] {new Point(395, 682),}));

            #endregion

            page = Pages[4];

            #region page4
            page.Add(CreteTenantInitial(new Point(70, 733)));
            page.Add(CreteTenantInitial(new Point(106, 733), "1"));
            page.Add(CreteLordInitial(new Point(200, 733)));
            page.Add(CreteLordInitial(new Point(235, 733), "1"));

            page.Add(new WordToReplace("LateAmount", new[] {new Point(490,44)}));
            page.Add(new WordToReplace("LateDays", new[] {new Point(283,55)}));
            page.Add(new WordToReplace("PetsDepositPaid", new[] {new Point(226,85)}));
            page.Add(new WordToReplace("PetsNumber", new[] {new Point(40,117)}));
            page.Add(new WordToReplace("PetsNoSmoking", new[] {new Point(104,146)}));
            page.Add(new WordToReplace("NoticesAgent", new[] {new Point(40,188)}));
            page.Add(new WordToReplace("NoticesLandlord", new[] {new Point(38,208)}));
            page.Add(new WordToReplace("NoticesLandlord1", new[] {new Point(95,209)}));
            page.Add(new WordToReplace("NoticesLandlordAt", new[] {new Point(335,209)}));
            page.Add(new WordToReplace("NoticesLandlordAgent", new[] {new Point(38,229)}));
            page.Add(new WordToReplace("NoticesLandlordAgent1", new[] {new Point(126,229)}));
            page.Add(new WordToReplace("NoticesLandlordAgentAt", new[] {new Point(335,229)}));
            page.Add(new WordToReplace("Utilities", new[] {new Point(265,333)}));
             
            page.Add(new WordToReplace("MaintenanceRoofs", new[] { new Point(52,409) }));
            page.Add(new WordToReplace("MaintenanceRoofs1", new[] { new Point(85, 409) })); 
            page.Add(new WordToReplace("MaintenanceWindows", new[] { new Point(195, 409) }));
            page.Add(new WordToReplace("MaintenanceWindows1", new[] { new Point(227, 409) }));
            page.Add(new WordToReplace("MaintenanceScreens", new[] { new Point(312, 409) }));
            page.Add(new WordToReplace("MaintenanceScreens1", new[] { new Point(344, 409) }));
            page.Add(new WordToReplace("MaintenanceSteps", new[] { new Point(443, 409) }));
            page.Add(new WordToReplace("MaintenanceSteps1", new[] { new Point(476, 409) })); 
            page.Add(new WordToReplace("MaintenanceDoors", new[] { new Point(52, 420) }));
            page.Add(new WordToReplace("MaintenanceDoors1", new[] { new Point(85,420) }));
            page.Add(new WordToReplace("MaintenanceFloors", new[] { new Point(195, 420) }));
            page.Add(new WordToReplace("MaintenanceFloors1", new[] { new Point(227, 420) }));
            page.Add(new WordToReplace("MaintenancePorches", new[] { new Point(312, 420) }));
            page.Add(new WordToReplace("MaintenancePorches1", new[] { new Point(344, 420) }));
            page.Add(new WordToReplace("MaintenanceExterior", new[] { new Point(443,420) }));
            page.Add(new WordToReplace("MaintenanceExterior1", new[] { new Point(476,420) })); 
            page.Add(new WordToReplace("MaintenanceFoundations", new[] { new Point(52,431) }));
            page.Add(new WordToReplace("MaintenanceFoundations1", new[] { new Point(85, 431) }));
            page.Add(new WordToReplace("MaintenancePlumbing", new[] { new Point(195, 431) }));
            page.Add(new WordToReplace("MaintenancePlumbing1", new[] { new Point(227, 431) }));
            page.Add(new WordToReplace("MaintenanceStructural", new[] { new Point(312, 431) }));
            page.Add(new WordToReplace("MaintenanceStructural1", new[] { new Point(344, 431) })); 
            page.Add(new WordToReplace("MaintenanceHeating", new[] { new Point(52, 441) }));
            page.Add(new WordToReplace("MaintenanceHeating1", new[] { new Point(85, 441) }));
            page.Add(new WordToReplace("MaintenanceHotWater", new[] { new Point(195,441) }));
            page.Add(new WordToReplace("MaintenanceHotWater1", new[] { new Point(227,441) }));
            page.Add(new WordToReplace("MaintenanceRunning", new[] { new Point(312,441) }));
            page.Add(new WordToReplace("MaintenanceRunning1", new[] { new Point(344,441) }));
            page.Add(new WordToReplace("MaintenanceLocks", new[] { new Point(443,441) }));
            page.Add(new WordToReplace("MaintenanceLocks1", new[] { new Point(476,441) })); 
            page.Add(new WordToReplace("MaintenanceElectrical", new[] { new Point(52, 451) })); 
            page.Add(new WordToReplace("MaintenanceElectrical1", new[] { new Point(85,451) })); 
            page.Add(new WordToReplace("MaintenanceCooling", new[] { new Point(312,451) }));
            page.Add(new WordToReplace("MaintenanceCooling1", new[] { new Point(344,451) }));
            page.Add(new WordToReplace("MaintenanceSmoke", new[] { new Point(443,451) }));
            page.Add(new WordToReplace("MaintenanceSmoke1", new[] { new Point(476,451) })); 

            page.Add(new WordToReplace("MaintenanceGarbage", new[] { new Point(52,472) }));
            page.Add(new WordToReplace("MaintenanceGarbage1", new[] { new Point(85,472) }));

            page.Add(new WordToReplace("MaintenanceRats", new[] { new Point(52,482) }));
            page.Add(new WordToReplace("MaintenanceRats1", new[] { new Point(85,482) }));

            page.Add(new WordToReplace("MaintenanceWood", new[] { new Point(52,493) }));
            page.Add(new WordToReplace("MaintenanceWood1", new[] { new Point(85,493) }));

            page.Add(new WordToReplace("MaintenanceLawn", new[] { new Point(52,503) }));
            page.Add(new WordToReplace("MaintenanceLawn1", new[] { new Point(85,503) }));
            page.Add(new WordToReplace("MaintenancePool", new[] { new Point(196,503) }));
            page.Add(new WordToReplace("MaintenancePool1", new[] { new Point(227,503) }));

            page.Add(new WordToReplace("MaintenanceWaterTreatment", new[] { new Point(52,514) }));
            page.Add(new WordToReplace("MaintenanceWaterTreatment1", new[] { new Point(85,514) }));
            page.Add(new WordToReplace("MaintenanceFilters", new[] { new Point(196,514) }));
            page.Add(new WordToReplace("MaintenanceFilters1", new[] { new Point(227,514) }));
            page.Add(new WordToReplace("MaintenanceFilters2", new[] { new Point(318,514) }));

            page.Add(new WordToReplace("MaintenanceCeilings", new[] { new Point(52,524) }));
            page.Add(new WordToReplace("MaintenanceCeilings1", new[] { new Point(85,524) }));
            page.Add(new WordToReplace("MaintenanceWalls", new[] { new Point(196,524) }));
            page.Add(new WordToReplace("MaintenanceWalls1", new[] { new Point(227,524) }));

            page.Add(new WordToReplace("MaintenanceOther", new[] { new Point(52,534) }));
            page.Add(new WordToReplace("MaintenanceOther1", new[] { new Point(85,534) }));
            page.Add(new WordToReplace("MaintenanceOther2", new[] { new Point(210,535) }));

            page.Add(new WordToReplace("MaintenanceTenantName", new[] { new Point(113,566) }));
            page.Add(new WordToReplace("MaintenanceTenantAddress", new[] { new Point(290,566) }));

            page.Add(new WordToReplace("MaintenanceTenantTelephone", new[] { new Point(235,599) }));

            page.Add(new WordToReplace("Assignment", new[] { new Point(196,628) }));

            page.Add(new WordToReplace("KeysSets", new[] { new Point(40,681) }));
            page.Add(new WordToReplace("KeysMail", new[] { new Point(40,692) }));
            page.Add(new WordToReplace("KeysGarage", new[] { new Point(40,703) })); 
            #endregion

            page = Pages[5];

            #region page5
            page.Add(CreteTenantInitial(new Point(70, 733)));
            page.Add(CreteTenantInitial(new Point(106, 733), "1"));
            page.Add(CreteLordInitial(new Point(200, 733)));
            page.Add(CreteLordInitial(new Point(235, 733), "1"));

            page.Add(new WordToReplace("KeysKeys", new[] { new Point(40,64) }));
            page.Add(new WordToReplace("KeysKeysTo", new[] { new Point(150, 64) }));
            page.Add(new WordToReplace("KeysRemote", new[] { new Point(40,75) }));
            page.Add(new WordToReplace("KeysRemoteTo", new[] { new Point(190, 75) }));
            page.Add(new WordToReplace("KeysElectronic", new[] { new Point(40, 85) }));
            page.Add(new WordToReplace("KeysElectronicTo", new[] { new Point(192, 85) }));
            page.Add(new WordToReplace("KeysElectronicTo1", new[] { new Point(40, 96) }));
            page.Add(new WordToReplace("KeysOtherTo", new[] { new Point(273,96) }));

            page.Add(new WordToReplace("KeysName", new[] { new Point(353,115) }));
            page.Add(new WordToReplace("KeysAddress", new[] { new Point(50,135) })); 



            page.Add(new WordToReplace("LeadBasedPaint", new[] { new Point(149, 163) }));

            page.Add(new WordToReplace("LeadBasedPaintAInitial", new[] { new Point(38, 265) }));
            page.Add(new WordToReplace("LeadBasedPaintAI", new[] { new Point(129, 275) }));
            page.Add(new WordToReplace("LeadBasedPaintAI1", new[] { new Point(38, 295) }));
            page.Add(new WordToReplace("LeadBasedPaintAI2", new[] { new Point(38, 315) }));
            page.Add(new WordToReplace("LeadBasedPaintAII", new[] { new Point(129, 334) }));
            page.Add(new WordToReplace("LeadBasedPaintBInitial", new[] { new Point(38, 344) }));
            page.Add(new WordToReplace("LeadBasedPaintBI", new[] { new Point(129, 354) }));
            page.Add(new WordToReplace("LeadBasedPaintBI1", new[] { new Point(38, 385) }));
            page.Add(new WordToReplace("LeadBasedPaintBI2", new[] { new Point(38, 404) }));
            page.Add(new WordToReplace("LeadBasedPaintBII", new[] { new Point(129, 422) }));


            page.Add(new WordToReplace("LeadBasedPaintCInitial", new[] { new Point(38, 473) }));
            page.Add(new WordToReplace("LeadBasedPaintDInitial", new[] { new Point(38, 484) }));

            page.Add(new WordToReplace("LeadBasedPaintEInitial", new[] { new Point(38, 524) }));

            page.Add(new WordToReplace("LessorsSignature", new[] { new Point(38, 596) }));
            page.Add(new WordToReplace("Date", new[] { new Point(215, 596) }));
            page.Add(new WordToReplace("LessorsSignature1", new[] { new Point(315, 596) }));
            page.Add(new WordToReplace("Date1", new[] { new Point(490, 596) }));

            page.Add(new WordToReplace("LessorsSignature2", new[] { new Point(38, 627) }));
            page.Add(new WordToReplace("Date2", new[] { new Point(215, 627) }));
            page.Add(new WordToReplace("LessorsSignature3", new[] { new Point(315, 627) }));
            page.Add(new WordToReplace("Date3", new[] { new Point(490, 627) }));

            page.Add(new WordToReplace("AgentsSignature", new[] { new Point(38, 659) }));
            page.Add(new WordToReplace("Date4", new[] { new Point(215, 659) }));
            page.Add(new WordToReplace("AgentsSignature1", new[] { new Point(315, 659) }));
            page.Add(new WordToReplace("Date5", new[] { new Point(490, 659) }));

            #endregion


            page = Pages[6];

            #region page6
            page.Add(CreteTenantInitial(new Point(70, 733)));
            page.Add(CreteTenantInitial(new Point(106, 733), "1"));
            page.Add(CreteLordInitial(new Point(200, 733)));
            page.Add(CreteLordInitial(new Point(235, 733), "1"));

            page.Add(new WordToReplace("HomeOwners", new[] { new Point(507,199) }));
            page.Add(new WordToReplace("HomeOwners1", new[] { new Point(38,209) }));
            page.Add(new WordToReplace("HomeOwners2", new[] { new Point(475,250) }));
            page.Add(new WordToReplace("HomeOwners3", new[] { new Point(533,250) }));
            page.Add(new WordToReplace("Premises", new[] { new Point(261,321) }));

            #endregion

            page = Pages[7];

            #region page7
            page.Add(CreteTenantInitial(new Point(70, 734)));
            page.Add(CreteTenantInitial(new Point(106, 734), "1"));
            page.Add(CreteLordInitial(new Point(200, 734)));
            page.Add(CreteLordInitial(new Point(235, 734), "1"));

            page.Add(new WordToReplace("Brokers", new[] { new Point(177,209) })); 
            page.Add(new WordToReplace("Brokers1", new[] { new Point(207,219) })); 
            page.Add(new WordToReplace("Brokers2", new[] { new Point(262,219) })); 
            page.Add(new WordToReplace("BrokersLicensee", new[] { new Point(40, 247) })); 
            page.Add(new WordToReplace("BrokersLicensee1", new[] { new Point(332, 247) })); 

            page.Add(new WordToReplace("BrokersCompany", new[] { new Point(40,278) })); 
            page.Add(new WordToReplace("BrokersCompany1", new[] { new Point(332,278) })); 

            page.Add(new WordToReplace("BrokersCommission", new[] { new Point(40,309) })); 
            page.Add(new WordToReplace("BrokersCommission1", new[] { new Point(332,309) }));  

             
            page.Add(new WordToReplace("TenantsPersonalProperty", new[] { new Point(378, 334) }));

            page.Add(new WordToReplace("LandlordSignature", new[] { new Point(40, 416) }));
            page.Add(new WordToReplace("LandLordDate", new[] { new Point(315, 416) }));

            page.Add(new WordToReplace("LandlordSignature1", new[] { new Point(40, 450) }));
            page.Add(new WordToReplace("LandLordDate1", new[] { new Point(315, 450) }));

            page.Add(new WordToReplace("LandlordSignature2", new[] { new Point(40, 485) }));
            page.Add(new WordToReplace("LandLordDate2", new[] { new Point(315, 485) }));

            page.Add(new WordToReplace("LandlordSignature3", new[] { new Point(40, 519) }));
            page.Add(new WordToReplace("LandLordDate3", new[] { new Point(315, 519) }));

            page.Add(new WordToReplace("LandlordSignature4", new[] { new Point(40,554) }));
            page.Add(new WordToReplace("LandLordDate4", new[] { new Point(315,554) }));

            page.Add(new WordToReplace("NameIndividual", new[] { new Point(145, 605) }));
            page.Add(new WordToReplace("NameBusiness", new[] { new Point(145, 617) }));
            page.Add(new WordToReplace("Address", new[] { new Point(145, 629) }));
            page.Add(new WordToReplace("TelephoneNumber", new[] { new Point(145, 641) })); 

            #endregion

            page = Pages[8];

            #region page8
            page.Add(CreteTenantInitial(new Point(70, 732)));
            page.Add(CreteTenantInitial(new Point(106, 732), "1"));
            page.Add(CreteLordInitial(new Point(200, 732)));
            page.Add(CreteLordInitial(new Point(235, 732), "1"));

            page.Add(new WordToReplace("Agree", new[] { new Point(37, 70) }));
            page.Add(new WordToReplace("Pay", new[] { new Point(265, 69) }));

            page.Add(new WordToReplace("NotAgree", new[] { new Point(37, 113) }));

            page.Add(new WordToReplace("EarlyLandLordSignature", new[] { new Point(40, 197) }));
            page.Add(new WordToReplace("EarlyLandLordDate", new[] { new Point(325, 197) }));

            page.Add(new WordToReplace("EarlyLandLordSignature1", new[] { new Point(40, 248) }));
            page.Add(new WordToReplace("EarlyLandLordDate1", new[] { new Point(325, 248) }));

            page.Add(new WordToReplace("EarlyLandLordSignature2", new[] { new Point(40, 300) }));
            page.Add(new WordToReplace("EarlyLandLordDate2", new[] { new Point(325, 300) }));

            page.Add(new WordToReplace("EarlyLandLordSignature3", new[] { new Point(40, 352) }));
            page.Add(new WordToReplace("EarlyLandLordDate3", new[] { new Point(325, 352) }));

            page.Add(new WordToReplace("EarlyLandLordSignature4", new[] { new Point(40, 404) }));
            page.Add(new WordToReplace("EarlyLandLordDate4", new[] { new Point(325, 404) }));

            #endregion

            page = Pages[9];

            #region page9

            page.Add(CreteTenantInitial(new Point(64, 748)));
            page.Add(CreteTenantInitial(new Point(101, 748), "1"));
            page.Add(CreteLordInitial(new Point(194, 748)));
            page.Add(CreteLordInitial(new Point(230, 748), "1"));

            #endregion

            page = Pages[10];

            #region page10

            page.Add(CreteTenantInitial(new Point(64, 748)));
            page.Add(CreteTenantInitial(new Point(101, 748), "1"));
            page.Add(CreteLordInitial(new Point(194, 748)));
            page.Add(CreteLordInitial(new Point(230, 748), "1"));

            #endregion


            page = Pages[11];

            #region page11

            page.Add(CreteTenantInitial(new Point(64, 747)));
            page.Add(CreteTenantInitial(new Point(101, 747), "1"));
            page.Add(CreteLordInitial(new Point(194, 747)));
            page.Add(CreteLordInitial(new Point(230, 747), "1"));

            page.Add(new WordToReplace("DepositMoney", new[] { new Point(345, 664) }));
            page.Add(new WordToReplace("DepositMoney1", new[] { new Point(520, 664) }));

            #endregion

            page = Pages[12];

            #region page12

            page.Add(CreteTenantInitial(new Point(64, 747)));
            page.Add(CreteTenantInitial(new Point(101, 747), "1"));
            page.Add(CreteLordInitial(new Point(194, 747)));
            page.Add(CreteLordInitial(new Point(230, 747), "1"));

            #endregion

            page = Pages[13];

            #region page13

            page.Add(CreteTenantInitial(new Point(64, 747)));
            page.Add(CreteTenantInitial(new Point(101, 747), "1"));
            page.Add(CreteLordInitial(new Point(194, 747)));
            page.Add(CreteLordInitial(new Point(230, 747), "1"));

            #endregion

            page = Pages[14];

            #region page14

            page.Add(CreteTenantInitial(new Point(64, 747)));
            page.Add(CreteTenantInitial(new Point(101, 747), "1"));
            page.Add(CreteLordInitial(new Point(194, 747)));
            page.Add(CreteLordInitial(new Point(230, 747), "1"));

            page.Add(new WordToReplace("TerminationRental", new[] { new Point(290, 424) }));

            #endregion

            page = Pages[15];

            #region page15

            page.Add(CreteTenantInitial(new Point(64, 747)));
            page.Add(CreteTenantInitial(new Point(101, 747), "1"));
            page.Add(CreteLordInitial(new Point(194, 747)));
            page.Add(CreteLordInitial(new Point(230, 747), "1"));

            #endregion

            page = Pages[16];

            #region page16

            page.Add(CreteTenantInitial(new Point(64, 747)));
            page.Add(CreteTenantInitial(new Point(101, 747), "1"));
            page.Add(CreteLordInitial(new Point(194, 747)));
            page.Add(CreteLordInitial(new Point(230, 747), "1"));

            page.Add(new WordToReplace("Choice", new[] { new Point(249, 364) }));

            #endregion

            page = Pages[17];

            #region page17

            page.Add(CreteTenantInitial(new Point(64, 753)));
            page.Add(CreteTenantInitial(new Point(101, 753), "1"));
            page.Add(CreteLordInitial(new Point(194, 753)));
            page.Add(CreteLordInitial(new Point(230, 753), "1"));

            #endregion

            page = Pages[18];

            #region page18

            page.Add(CreteTenantInitial(new Point(64, 748)));
            page.Add(CreteTenantInitial(new Point(101, 748), "1"));
            page.Add(CreteLordInitial(new Point(194, 748)));
            page.Add(CreteLordInitial(new Point(230, 748), "1"));

            #endregion

            page = Pages[19];

            #region page19

            page.Add(CreteTenantInitial(new Point(64, 747)));
            page.Add(CreteTenantInitial(new Point(101, 747), "1"));
            page.Add(CreteLordInitial(new Point(194, 747)));
            page.Add(CreteLordInitial(new Point(230, 747), "1"));

            #endregion
        }
    }
}